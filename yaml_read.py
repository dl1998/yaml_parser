import yaml


class PathParser:
    def __init__(self, dictionary):
        self.__dictionary = dictionary['project']
        self.__paths = {}

    def __find_paths(self, elements, path=''):
        for key, element in elements.items():
            if isinstance(element, dict):
                attribute = ''
                if element['root']:
                    attribute = element['root']
                self.__find_paths(element, path + attribute)
            else:
                if key != 'root' and key != 'name':
                    self.__paths[key] = path + element
        return True

    @property
    def paths(self):
        self.__find_paths(self.__dictionary)
        return self.__paths


with open('text.yml', 'r') as file:
    yaml_file = yaml.safe_load(file)
    # print(yaml_file)
    path_parser = PathParser(yaml_file)
    paths = path_parser.paths
    for name, path in paths.items():
        print(f'{name}: {path}')
